package com.myzee;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SingletonWithSerialization{

     public static void main(String []args) throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectOutput out = new ObjectOutputStream(new FileOutputStream("myzee.ser"));
        Student s = Student.getInstance();
        out.writeObject(s);
        out.close();
        
        ObjectInput in = new ObjectInputStream(new FileInputStream(
                "myzee.ser"));
        Student s1 = (Student)in.readObject();
        System.out.println(s.hashCode() + "\n" + s1.hashCode());
        in.close();
        
        
     }
}

class Student implements Serializable {
    private int id;
    private String name;
    private static Student s;
    private Student(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public static Student getInstance() {
        if(s == null) {
            return new Student(12, "nagaraj");
        } else {
            return s;
        }
    }
    
    @Override
    protected Object readResolve() {
        return getInstance();
    }
    
}